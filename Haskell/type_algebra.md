Canonical representation of a type

%

Any type of the form \(t = \sum\limits_m\prod\limits_n t_{m,n} \)

(including e.g. `()`, `a -> b`, and even, typically, `(a, Int)`)