Does the following type declaration have a `Functor` instance?

```haskell
newtype T1 a = T1 (Int -> a)
```

%

Yes

---

Does the following type declaration have a `Functor` instance?

```haskell
newtype T2 a = T2 (a -> Int)
```

%

No

---



Does the following type declaration have a `Functor` instance?

```haskell
newtype T3 a = T3 ((a -> Int) -> Int)
```

%

Yes

