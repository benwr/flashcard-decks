If `Show Int` has kind `CONSTRAINT` , what’s the kind of `Show`?

%

`TYPE -> CONSTRAINT`

---

What is the kind of `Functor`?

%

`(TYPE -> TYPE) -> CONSTRAINT`

---

What is the kind of `MonadTrans`?

%

`((TYPE -> TYPE) -> (TYPE -> TYPE)) -> CONSTRAINT`
