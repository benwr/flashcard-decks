What extension enables _promoted data constructors_ like `'True` and `'False`?

%

`DataKinds`

---

with `DataKinds`, what is the type-level (promoted) equivalent of a `String`, and how do you use it?

%

`SYMBOL`s can be used by specifying a string literal anywhere that a type is expected; e.g. `:kind "hello"` gives `"hello" :: Symbol`

---

When building a promoted list with `DataKinds`, what syntax is important to remember?

%

`'['True]` is a parse error;  you want `'[ 'True ]`