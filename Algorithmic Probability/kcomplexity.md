English description of the Kolmogorov Complexity of a string of bits

%

Given a string \(s\), its Kolmogorov complexity \(K(s)\), relative to some prefix-free universal Turing machine with unidirectional input and output, is the length of the shortest input that produces \(s\).

---

Mathematical definition of Kolmogorov complexity

%

Given a prefix-free universal turing machine \(U\), the Kolmogorov complexity is

\[K(s) = \min\limits_{p \in B | U(p) = s}\ell(p)\]

where \(B\) is the set of finite strings of bits.