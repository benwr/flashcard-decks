Define Semimeasure

%

A subadditive function to the positive reals

---

Define subadditive

%

Of a function \(f : \mathbb{R} \to \mathbb{R}\), the property that \(f(a + b)\)