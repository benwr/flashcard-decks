Newton's First Law

%

Objects moving with a particular velocity tend
to continue moving with that velocity unless acted
upon by an outside force

---

Newton's Second Law

%

\(F = ma\)

---

Newton's Third Law

%

Whenever an object \(A\) exerts a force \(F\) on object \(B\),
\(B\) exerts an force \(-F\) on \(A\)
