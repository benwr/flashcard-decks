Avogadro's Number

%

\(6.022 \times 10^{23}\)

---

Gravitational Constant

%

\(6.67 \times 10^{-11} \frac{m^3}{kg\cdot s^2}\)