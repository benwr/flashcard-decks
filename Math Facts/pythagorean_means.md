List the pythagorean means

%

* Harmonic
* Geometric
* Arithmetic

---

Inequalities among pythagorean means, assuming all arguments are positive.

%

\[\min \le H \le G \le A \le \max\]

---

Properties of the pythagorean means

%

* Value Preservation: \(M(x,\ldots,x) = x\)
* First-order homogeneity: \(M(b\vec{x}) = bM(\vec{x})\)
* Invariance under exchange: \(M(\ldots, x_i,x_j,\ldots) = M(\ldots,x_j,x_i,\ldots)\)
* Averaging: \(\min(\vec{x}) \le M(\vec{x}) \le \max(\vec{x})\)

---

How can the the harmonic mean of two positive real numbers related geometrically to the arithmetic, geometric, arithmetic, and quadratic means of those numbers?

%

![](images/MathematicalMeans.png)

(where the triangle is a right triangle )