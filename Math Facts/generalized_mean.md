Definition of the generalized mean

%

\(G(p, \vec{x}) = \left(\sum\limits_{i=0}^{|x|} x_i^p\right)^{\frac{1}{p}}\)