Assuming \(b > 0\), what is \(\log_b(1)\) ?

%

\(0\), since \(b^0=1\)

---

Assuming \(b > 0\) and \(b \ne 1\), what is \(\log_b(b)\)?

%

\(1\), since \(b^1 = b\)

---

What is \(b^{\log_b(x)}\) (assuming \(b \notin \{0, 1\}\) and \(x \ne 0\))?

%

\(x\)

---

What is \(\log_b{(b^{x})}\) (assuming \(b \notin \{0, 1\}\) and \(x \ne 0\))?

%

\(x\)

---

