Definition of the harmonic mean

%

\[H = \frac{n}{\sum\limits_{i = 1}^n{x_i^{-1}}}\]


---

How is the harmonic mean related to the arithmetic mean?

%

\[H = \frac{n}{\sum\limits_{i = 1}^n{x_i^{-1}}} = \left(\frac{\sum\limits_{i = 1}^n{x_i^{-1}}}{n}\right)^{-1}\]

Therefore the harmonic mean is the reciprocal of the arithmetic mean of the reciprocals of the arguments - it is the _reciprocal dual_ of the arithmetic mean.
