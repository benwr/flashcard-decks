Find the area of the polygon in this image:

![](images/polygon.jpg)

%

If you know the vertices of a polygon, here’s an interesting way to find its area:

1.   Arrange the vertices in a vertical list, repeating the first vertex at the end (see below).
2.   Multiply diagonally downward both ways as shown.
3.   Add the products on each side.
4.   Find the difference of these sums.
5.   Halve that difference to get the area.

![](images/polygon.jpg)

![](images/area_table.jpg)