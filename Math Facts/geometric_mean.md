Definition of the geometric mean

%

\[G(\vec{x}) = \sqrt[n]{\left|\prod\limits_{i=0}^{n-1}x_i\right|}\]

---

What is the reciprocal dual of the geometric mean?

%

The geometric mean itself:

\[(G(x_0^{-1},\ldots,x_n^{-1}))^{-1} = G(x_0, \ldots, xn)\]
